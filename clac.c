#include <stdio.h>
#include <math.h>

float factorial(float m)
{
    if (m==0 || m==1) return (1);
    else      return (m*factorial(m-1));
}
void simpleMathOperation();
void calculateThreeLarge();
void calculateSinCosExpTaylor();
void sumOfNumbersOfNumber();
void primeNumbers();
void SINT(float a,float b);
void COST(float a,float b);
void EXPT(float a,float b);

int main()
{
    printf("------------welcome to calc------------\n");
	int item;
	do
    {
        printf("1.Simple mathematical operations\n");
        printf("2.Calculate the three larger\n");
        printf("3.Calculate Sin & Cos & exp via teylor\n");
        printf("4.a number's numbers sum\n");
        printf("5.prime numbers\n");
        printf("0.Exit\n");
        printf("Select an item:\n");
        scanf("%d" , &item);
        switch (item)
        {
            case 0 :
                {
                    printf("\n----------thanks to use----------\n");
                    break;
                }
            case 1 :
                {
                    simpleMathOperation();
                    break;
                }
            case 2 :
                {
                    calculateThreeLarge();
                    break;
                }
            case 3 :
                {
                    calculateSinCosExpTaylor();
                    break;
                }
            case 4 :
                {
                    sumOfNumbersOfNumber();
                    break;
                }
            case 5 :
                {
                    primeNumbers();
                    break;
                }
            default :  printf("\n!!!! please enter a number from upper list !!!! :) \n");
        }
    }while (item != 0);
	return 0;
}

void simpleMathOperation()
{
    printf("\n.........Simple mathematical operations.........\n");
    char op;
    float a , b ,c;
    fseek(stdin,0,SEEK_END);
    scanf("%c", &op);
    while (op != 'q')
    {
        scanf("%f %f",&a, &b);
        switch (op)
        {
            case '+' :
                {
                    c = a + b;
                    break;
                }
            case '-' :
                {
                    c = a - b;
                    break;
                }
            case '*' :
                {
                    c = a * b;
                    break;
                }
            case '/' :
                {
                    c = a / b;
                    break;
                }
            case '%' :
                {
                    c = (int)a % (int)b;
                    break;
                }
        }
        if ( c-(int)c == 0 )
        {
            printf("%d \n" , (int)c);
        }else{
            printf("%.2f \n" , c);
        }
        fseek(stdin,0,SEEK_END);
        scanf("%c" , &op);
    }
    printf("\n.........................\n");
}

void calculateThreeLarge()
{
    printf("\n.........Calculate the three larger.........\n");
    int n;
    float a = 0, b = 0, c = 0, d = 0;
    fseek(stdin,0,SEEK_END);
    scanf("%d" , &n);
    while (n != 0)
    {
       while (n > 0)
        {
            scanf("%f" , &a);
            if(a > b)
            {
                d = c;
                c = b;
                b = a;
            }
            else if(a > c)
            {
                d = c;
                c = a;
            }
            else if(a > d)
            {
                d = a;
            }
            n--;
        }
        if (d-(int)d == 0 && c-(int)c == 0 && b-(int)b == 0)
        {
            printf("%d %d %d \n",(int)b ,(int)c ,(int)d);
        }
        else if(d-(int)d == 0 && c-(int)c == 0 && b-(int)b != 0)
        {
            printf("%.2f %d %d \n",b ,(int)c ,(int)d);
        }
        else if(d-(int)d == 0 && c-(int)c != 0 && b-(int)b == 0)
        {
            printf("%d %.2f %d \n",(int)b ,c ,(int)d);
        }
        else if(d-(int)d != 0 && c-(int)c == 0 && b-(int)b == 0)
        {
            printf("%d %d %.2f \n",(int)b ,(int)c ,d);
        }
        else if(d-(int)d == 0 && c-(int)c != 0 && b-(int)b != 0)
        {
            printf("%.2f %.2f %d \n",b ,c ,(int)d);
        }
        else if(d-(int)d != 0 && c-(int)c == 0 && b-(int)b != 0)
        {
            printf("%.2f %d %.2f \n",b ,(int)c ,d);
        }
        else if(d-(int)d != 0 && c-(int)c != 0 && b-(int)b == 0)
        {
            printf("%d %.2f %.2f \n",(int)b ,c ,d);
        }
        else if(d-(int)d != 0 && c-(int)c != 0 && b-(int)b != 0)
        {
            printf("%.2f %.2f %.2f \n",b ,c ,d);
        }
        fseek(stdin,0,SEEK_END);
        scanf("%d" , &n);
        a = 0;b = 0;c = 0; d = 0;
    }
    printf("\n.........................\n");
}

void  SINT(float a,float b)
{
    float s = 0;
    for (int i=1; i < b ; i += 2)
    {
        if (((i-1)/2)%2 == 0)
        {
           s = s + pow(a , i)/factorial(i);
        }
        else
        {
            s = s - pow(a , i)/factorial(i);
        }
    }
    if ( s-(int)s ==0 )
    {
        printf("%d \n" , (int)s);
    }else{
        printf("%.2f \n" , s);
    }
    printf("\n.........................\n");
}
void  COST(float a,float b)
{
    float c;
    for (int i=0; i < b ; i += 2)
    {
        if (((i)/2)%2 == 0)
        {
            c = c + pow(a , i)/factorial(i);
        }
        else
        {
            c = c - pow(a , i)/factorial(i);
        }
    }
    if ( c-(int)c ==0)
    {
        printf("%d \n" , (int)c);
    }else{
        printf("%.2f \n" , c);
    }
    printf("\n.........................\n");
}
void  EXPT(float a,float b)
{
    float e;
    for (int i=0; i < b ; i++)
    {
        e = e + pow(a , i)/factorial(i);
    }
    if ( e-(int)e ==0 )
    {
        printf("%d\n" , (int)e);
    }else{
        printf("%.2f\n" , e);
    }
    printf("\n.........................\n");
}

void calculateSinCosExpTaylor()
{
    printf("\n.........Calculate Sin & Cos & exp via teylor.........\n");
    char op;
    float a, b;
    scanf("%c",&op);
    while (op != 'q')
    {
        scanf("%f %f", &a, &b);
        switch (op)
        {
            case 's' :
                {
                    SINT((a * M_PI / 180),b);
                    break;
                }
            case 'c' :
                {
                    COST((a * M_PI / 180),b);
                    break;
                }
            case 'e' :
                {
                    EXPT(a,b);
                    break;
                }
        }
        scanf("%c",&op);
    }

}

void sumOfNumbersOfNumber()
{
    printf("\n.........a number's numbers sum.........\n");
    int n, m = 0;
    scanf("%d" , &n);
    while(n != 0)
    {
        while (n > 0) {
           m = m + n % 10;
           n = n / 10;
        }
        printf("%d \n" , m);
        scanf("%d" , &n);
        m = 0;
    }
    printf("\n.........................\n");
}

void primeNumbers()
{
    printf("\n.........prime numbers.........\n");
    int n ,ct=0;
    scanf("%d" , &n);
    while(n != 0)
    {
        for(int i=2;i<=n;i++)
         {
            ct=0;
            for(int j=2;j<i;j++)
            {
                if(i%j==0)
                {
                    ct=1;
                    break;
                }
            }
            if(ct==0)
            {
                printf("%d ",i);
            }
         }
         printf("\n");
         scanf("%d" , &n);
    }
    printf("\n.........................\n");
}
